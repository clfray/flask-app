from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(1, 2)
    host = "http://app:8000"
    # def on_start(self):
    #     self.client.post("/login", json={"username":"foo", "password":"bar"})

    @task
    def api_items(self):
        self.client.get("/api/items")

    @task
    def api_catalog(self):
        self.client.get("/api/catalog")