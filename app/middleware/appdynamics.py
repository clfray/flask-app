import logging
from werkzeug.wrappers import Request, Response
from appdynamics.agent import api as appd
from sqlalchemy.engine import Engine
from sqlalchemy.event import listen
from sqlalchemy.pool import Pool

class AppDynamics:

    def __init__(self, app):
        logging.info('MIDDLEWARE INIT')
        listen(Engine, "before_cursor_execute", self.before_cursor_execute)
        listen(Engine, "after_cursor_execute", self.after_cursor_execute)
        listen(Engine, "handle_error", self.handle_error)
        appd.init()
        self.bt_handle = None
        self.req = None
        self.app = app
        self.exit_call = None

    def __call__(self, environ, start_response):
        self.req = Request(environ, shallow=True)
        return self.app(environ, start_response)

    def on_connect(self, conn, *args):
        logging.warn("New DB connection:", conn)

    def before_cursor_execute(self, conn, cursor, statement, parameters, context, executemany):
        
        id_props = {
            'Host': conn.engine.url.host, 
            'Port': conn.engine.url.port,
            'Vendor': conn.engine.url.database
        }

        name = f'sql://{conn.engine.url.host}:{conn.engine.url.port}:{conn.engine.url.database}'

        # self.bt_handle = appd.start_bt(self.req.environ['PATH_INFO'])
        
        self.bt_handle = appd.get_active_bt_handle(self.req)
        self.exit_call = appd.start_exit_call(self.bt_handle, appd.EXIT_DB, name, id_props, operation=statement)


    def after_cursor_execute(self, conn, cursor, statement, parameters, context, executemany):
        # logging.warn("middleware after cursor execution", self.exit_call)
        if self.exit_call:
            appd.end_exit_call(self.exit_call)

    def handle_error(self, context):
        # logging.warn("middleware handle error", self.exit_call)
        if self.exit_call:
            appd.end_exit_call(self.exit_call, context)