from flask import Flask, session, request
from flask_sqlalchemy import SQLAlchemy
from config import config
import logging

from appdynamics.agent import api as appd
from app.middleware.appdynamics import AppDynamics

db = SQLAlchemy()

def create_app(config_name):
    
    app = Flask(__name__)
    app.wsgi_app = AppDynamics(app.wsgi_app)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)

    from app.api.routes import api as api_blueprint
    app.register_blueprint(api_blueprint)

    import logging
    import datetime
    gunicorn_error_logger = logging.getLogger('gunicorn.error')

    app.logger.addHandler(logging.StreamHandler())
    app.logger.handlers.extend(gunicorn_error_logger.handlers)
    app.logger.setLevel(logging.DEBUG)
    
    # listen(Pool, 'connect', appd_on_connect)
  


    app.logger.info(
        f'!!!!!!!!!!!!!!! Flask Application Start {datetime.datetime.now()} !!!!!!!!!!!!!!!!!!!!')

    
    return app
