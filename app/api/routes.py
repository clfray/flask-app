from appdynamics.agent import api as appd
from flask import jsonify, Blueprint, request
from app import db
import random
import time

api = Blueprint('api', __name__)


@api.route('/api/items')
def items():
    data = []
    global exc
    exc = None
    try:
        r = db.session.execute("select * from items")
        for row in r:
            data.append({'name': row.name,
                'description': row.description})
    except Exception as e:
        raise Exception(f'There was an error')
    finally:
        db.session.close()
    return jsonify({'status': 'App Working', 'data': data})


@api.route('/api/catalog')
def catalog_new():
    data = []
    try:
        num = random.randrange(0,9)
        sql = "select * from catalog"

        if num == 1:
            sql = "select * from catalsog"
        if num == 3:
            time.sleep(20)
        
        r = db.session.execute(sql)
        for row in r:
            data.append({'name': row.name,
                'description': row.description})
    except Exception as e:
        raise Exception(f'There was an error with the database query')
    finally:
        db.session.close()
    return jsonify({'status': 'App Working', 'data': data})


@api.route('/api/catalog_old')
def catalog():

    data = []
    

    # Set the identifying properties
    ID_PROPS = {'Host': 'db', 'Port': 1433, 'Vendor': 'catalog db'}

    with appd.bt('/api/catalog') as bt_handle:
        # Start the exit calli
        # num = random.randrange(0, 9)
        # if num in [1,4]:
        #     db_query = 'exec getCatalog'
        # else:
        #     db_query = 'select * from catalog'
        db_query = 'select * from catalog'

        exit_call = appd.start_exit_call(
            bt_handle, appd.EXIT_DB, 'Catalog DB',ID_PROPS, operation=db_query)
        
        global exc
        exc = None

        try:
            #  Start the exit calli
            num = random.randrange(0, 9)
            if num in [1]:
                time.sleep(15)
            r = db.session.execute(db_query)
            for row in r:
                data.append({'name':row.name, 'description': row.description})
        except Exception as e:
            exc = e
            # Assuming something above handles exceptions for you
            raise Exception(f'An Appication Error Was Experienced: {exc}')
        finally:
            # End the exit call
            print(f'EEEEEEEEEEEEEEE - {exc}')
            appd.end_exit_call(exit_call, exc)
            db.session.close()

    return jsonify({'status': 'Working', 'data': data})
